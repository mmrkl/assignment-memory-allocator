CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
TESTDIR=$(SRCDIR)/test
CC=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o \
	$(BUILDDIR)/test_simple_allocate_and_free.o \
	$(BUILDDIR)/multiple_allocate_and_free.o \
	$(BUILDDIR)/memory_is_over_expand_the_region.o \
	$(BUILDDIR)/memory_is_over_allocate_new_region.o \
	$(BUILDDIR)/test.o \
	$(BUILDDIR)/main.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/test.o: $(TESTDIR)/test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/memory_is_over_allocate_new_region.o: $(TESTDIR)/memory_is_over_allocate_new_region.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/memory_is_over_expand_the_region.o: $(TESTDIR)/memory_is_over_expand_the_region.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/multiple_allocate_and_free.o: $(TESTDIR)/multiple_allocate_and_free.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test_simple_allocate_and_free.o: $(TESTDIR)/simple_allocate_and_free.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

