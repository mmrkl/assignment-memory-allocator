#include "test/test.h"
#include "mem.h"

#define NDEBUG

void test(
    void (*test_func)(const struct block_header*), 
    const struct block_header* heap_heap, 
    const char* test_name) {
    debug("\nSTART TESTING > %s\n", test_name);
    test_func(heap_heap);
    debug("TEST SUCCEEDED > %s\n", test_name);
}

int main(void) {
    debug("\nSTART INITING HEAP\n");
    const struct block_header* heap_heap = init();
    debug("SUCCESSFULLY INITING HEAP\n");

    test(test_simple_allocate_and_free, heap_heap, "test_simple_allocate_and_free");
    test(test_multiple_allocate_and_free, heap_heap, "test_multiple_allocate_and_free");
    test(test_memory_is_over_expand_the_region, heap_heap, "test_memory_is_over_expand_the_region");
    test(test_memory_is_over_allocate_new_region, heap_heap, "test_memory_is_over_allocate_new_region");

    return 0;
}