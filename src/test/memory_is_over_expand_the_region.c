#include "test.h"
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void test_memory_is_over_expand_the_region(const struct block_header* heap_head) {
    debug_heap(stdout, heap_head);

    size_t size = 4096;
    char* first = (char *) _malloc(sizeof(char) * REGION_MIN_SIZE);
    struct block_header *header = block_get_header(first);
    assert(header->next == NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == REGION_MIN_SIZE);

    debug_heap(stdout, heap_head);

    char* second = (char *) _malloc(sizeof(char) * size);
    header = block_get_header(second);
    assert(header->next != NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == size);

    memset(first, 0, REGION_MIN_SIZE);
    memset(second, 0, size);
    memcpy(first, "hello world!", 12);
    memcpy(second, "These violent delights have violent ends.", 41);

    debug_heap(stdout, heap_head);
    debug("first: %s\n", first);
    debug("second: %s\n", second);

    _free(second);
    _free(first);
    debug_heap(stdout, heap_head);
}