#include "test.h"
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void test_simple_allocate_and_free(const struct block_header* heap_head) {
    debug_heap(stdout, heap_head);
    
    int size = 100;
    char* array = (char *) _malloc(sizeof(char) * size);
    
    struct block_header *header = block_get_header(array);
    assert(header->next != NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == 100);

    memset(array, 0, size);
    memcpy(array, "hello world!", 12);

    debug_heap(stdout, heap_head);
    debug("Array: %s\n", array);

    _free(array);
    debug_heap(stdout, heap_head);
}