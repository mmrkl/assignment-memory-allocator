#ifndef _TEST_H_
#define _TEST_H_

struct block_header* init();
void test_simple_allocate_and_free(const struct block_header*);
void test_multiple_allocate_and_free(const struct block_header*);
void test_memory_is_over_expand_the_region(const struct block_header*);
void test_memory_is_over_allocate_new_region(const struct block_header*);

#endif
