#include "test.h"
#include "../mem_internals.h"
#include "../mem.h"
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>

struct block_header* init() {
    struct block_header* heap_head = (struct block_header*) heap_init( 2 * 4096 );
    assert(heap_head != NULL);
    assert(heap_head->next == NULL);
    assert(heap_head->is_free == true);
    assert(heap_head->capacity.bytes > 0);
    return heap_head;
}