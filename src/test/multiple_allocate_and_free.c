#include "test.h"
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void test_multiple_allocate_and_free(const struct block_header* heap_head) {
    debug_heap(stdout, heap_head);

    size_t size = 100;
    char* first = (char *) _malloc(sizeof(char) * size);
    char* second = (char *) _malloc(sizeof(char) * size);

    struct block_header *header = block_get_header(first);
    assert(header->next != NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == size);
    
    header = block_get_header(second);
    assert(header->next != NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == size);

    memset(first, 0, size);
    memset(second, 0, size);
    memcpy(first, "hello world!", 12);
    memcpy(second, "These violent delights have violent ends.", 41);

    debug_heap(stdout, heap_head);
    debug("first: %s\n", first);
    debug("second: %s\n", second);

    _free(second);
    _free(first);
    debug_heap(stdout, heap_head);
}