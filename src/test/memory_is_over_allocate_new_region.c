#include "test.h"
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void test_memory_is_over_allocate_new_region(const struct block_header* heap_head) {
    debug_heap(stdout, heap_head);

    size_t size = 4096;
    char* first = (char *) _malloc(sizeof(char) * REGION_MIN_SIZE);
    struct block_header *header = block_get_header(first);
    assert(header->is_free == false);
    assert(header->capacity.bytes == REGION_MIN_SIZE);

    debug_heap(stdout, heap_head);

    char* second = (char *) _malloc(sizeof(char) * REGION_MIN_SIZE);
    header = block_get_header(second);
    assert(header->next == NULL);
    assert(header->is_free == false);
    assert(header->capacity.bytes == REGION_MIN_SIZE);

    debug_heap(stdout, heap_head);

    block_size block_sz = size_from_capacity(header->capacity);
    struct region noise_region = alloc_region(header + block_sz.bytes, REGION_MIN_SIZE);
    assert(!region_is_invalid(&noise_region));

    char* third = (char *) _malloc(sizeof(char) * size);

    assert(noise_region.addr != third);

    memset(first, 0, REGION_MIN_SIZE);
    memset(second, 0, REGION_MIN_SIZE);
    memset(third, 0, size);
    memcpy(first, "hello world!", 12);
    memcpy(second, "These violent delights have violent ends.", 41);
    memcpy(third, "And in their triumph die, like fire and powder, Which as they kiss consume.", 75);

    debug_heap(stdout, heap_head);
    debug("first: %s\n", first);
    debug("second: %s\n", second);
    debug("third: %s\n", third);
    debug_heap(stdout, noise_region.addr);

    _free(second);
    _free(first);
    debug_heap(stdout, heap_head);
}