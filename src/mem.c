#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
struct region alloc_region ( void const * addr, size_t query ) {
  size_t actial_size = region_actual_size(query);
  block_capacity cap = {actial_size};
  block_size block_sz = size_from_capacity(cap);

  void* result = map_pages(addr, block_sz.bytes, 0);
  if (result == MAP_FAILED) {
    return REGION_INVALID;
  }

  block_init(result, block_sz, NULL);

  struct region aregion = {0};
  aregion.addr = result;
  aregion.size = actial_size;
  aregion.extends = true;

  return aregion;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_is_big_enough(query, block)) return false;
  if (!block_splittable(block, query)) return false;
  

  block_size next_cap = { block->capacity.bytes - query };
  block->capacity.bytes = query;
  struct block_header* next = block_after(block);
  block_init(next, next_cap, block->next);
  block->next = next;

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* nextElem = NULL;
  bool mergible = false;
  block_size new_size = {0};
  while(block->next != NULL) {
    if (!mergeable(block, block->next)) {
      break;
    }
    mergible = true;
    nextElem = block->next;
    new_size = size_from_capacity(nextElem->capacity); 
    block->capacity.bytes += new_size.bytes;
    block->next = nextElem->next;
  }
  return mergible;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
  struct block_search_result result = {0};
  if (block == NULL) {
    result.type = BSR_CORRUPTED;
    result.block = NULL;
    return result;
  }
  struct block_header* current = block;
  for(; current->next != NULL; current = current->next) {
    try_merge_with_next(current);
    if (current->is_free && block_is_big_enough(sz, current) ) {
      result.type = BSR_FOUND_GOOD_BLOCK;
      result.block = current;
      return result;
    }
  }
  if (current->is_free && block_is_big_enough(sz, current)) {
      result.type = BSR_FOUND_GOOD_BLOCK;
      result.block = current;
      return result;
  }
  result.type = BSR_REACHED_END_NOT_FOUND;
  result.block = current;
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  const struct region region = alloc_region( block_after(last) , query );
  if (region_is_invalid(&region)) {
    return NULL;
  }
  last->next = region.addr;
  return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type == BSR_FOUND_GOOD_BLOCK) return result.block;
  
  struct block_header* growed_heap_start = grow_heap(result.block, query);
  result = try_memalloc_existing(query, growed_heap_start);
  
  if (result.type == BSR_FOUND_GOOD_BLOCK) return result.block;
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header)); 
}
